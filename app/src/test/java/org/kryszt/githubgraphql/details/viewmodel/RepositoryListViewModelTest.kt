package org.kryszt.githubgraphql.details.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.apollographql.apollo.api.Response
import com.apollographql.apollo.exception.ApolloException
import com.google.common.truth.Truth
import com.nhaarman.mockitokotlin2.given
import com.nhaarman.mockitokotlin2.mock
import io.reactivex.Observable
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.koin.core.context.startKoin
import org.koin.core.parameter.parametersOf
import org.koin.test.AutoCloseKoinTest
import org.koin.test.inject
import org.koin.test.mock.declareMock
import org.kryszt.githubgraphql.RepositoriesQuery
import org.kryszt.githubgraphql.common.DataState
import org.kryszt.githubgraphql.common.di.commonModule
import org.kryszt.githubgraphql.details.di.detailsModule
import org.kryszt.githubgraphql.common.network.di.networkModule
import org.kryszt.githubgraphql.common.network.error.NoContentException
import org.kryszt.githubgraphql.common.network.error.ResponseError
import org.kryszt.githubgraphql.common.network.error.UnknownErrorException
import org.kryszt.githubgraphql.repositories.di.repositoriesModule
import org.kryszt.githubgraphql.repositories.entities.*
import org.kryszt.githubgraphql.repositories.viewmodel.RepositoryListViewModel
import org.kryszt.githubgraphql.rules.TrampolineSchedulerRule
import org.mockito.ArgumentMatchers
import com.apollographql.apollo.api.Error as ApolloError

class RepositoryListViewModelTest : AutoCloseKoinTest() {

    @get:Rule
    val rxSchedulersRule = TrampolineSchedulerRule()
    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()


    private val sut: RepositoryListViewModel by inject { parametersOf(OWNER) }

    @Before
    fun before() {
        startKoin {
            modules(listOf(commonModule, networkModule, repositoriesModule, detailsModule))
        }
    }

    @Test
    fun `should set error state on apollo error`() {
        setupMock(exceptionResponse(ApolloException("Connection error")))

        assertErrorResult {
            Truth.assertThat(throwable).isInstanceOf(ApolloException::class.java)
            Truth.assertThat(throwable.message).isEqualTo("Connection error")
        }
    }

    @Test
    fun `should set error state on error in response message`() {
        setupMock(errorResponse(ApolloError("Error Message", null, null)))

        assertErrorResult {
            Truth.assertThat(throwable).isInstanceOf(ResponseError::class.java)
            Truth.assertThat(throwable.message).isEqualTo("Error Message")
        }
    }

    @Test
    fun `should set fallback error message if one wasn't available in error`() {
        setupMock(errorResponse(ApolloError(null, null, null)))

        assertErrorResult {
            Truth.assertThat(throwable).isInstanceOf(UnknownErrorException::class.java)
        }
    }

    @Test
    fun `should set error state on empty response`() {
        setupMock(emptyResponse())

        assertErrorResult {
            Truth.assertThat(throwable).isInstanceOf(NoContentException::class.java)
        }
    }

    @Test
    fun `should set error state on response without repository`() {
        setupMock(noRepository())

        assertErrorResult {
            Truth.assertThat(throwable).isInstanceOf(NoContentException::class.java)
        }
    }

    @Test
    fun `should provide data on valid response`() {
        setupMock(withRepository())

        Truth.assertThat(sut.repositoriesState.value).isEqualTo(DataState.Ready(validResponse))
    }

    private fun assertErrorResult(testFunction: DataState.Error<*>.() -> Unit) {
        (sut.repositoriesState.value as? DataState.Error)
            ?.testFunction()
            ?: run { Assert.fail("Not an error result") }
    }

    private fun setupMock(observable: Observable<Response<RepositoriesQuery.Data>>) {
        declareMock<RepositoryListDataSource> {
            given(
                loadRepositories(
                    ArgumentMatchers.matches(OWNER),
                    ArgumentMatchers.eq(50),
                    ArgumentMatchers.isNull()
                )
            ).will { observable }
        }
    }

    private fun exceptionResponse(error: Throwable): Observable<Response<RepositoriesQuery.Data>> =
        Observable.error(error)

    private fun errorResponse(error: ApolloError): Observable<Response<RepositoriesQuery.Data>> =
        Response.builder<RepositoriesQuery.Data>(mock())
            .errors(listOf(error))
            .build()
            .let { Observable.just(it) }

    private fun emptyResponse(): Observable<Response<RepositoriesQuery.Data>> =
        Response.builder<RepositoriesQuery.Data>(mock())
            .data(null)
            .build()
            .let { Observable.just(it) }

    private fun noRepository(): Observable<Response<RepositoriesQuery.Data>> =
        Response.builder<RepositoriesQuery.Data>(mock())
            .data(RepositoriesQuery.Data(null))
            .build()
            .let { Observable.just(it) }

    private fun withRepository(): Observable<Response<RepositoriesQuery.Data>> =
        Response.builder<RepositoriesQuery.Data>(mock())
            .data(RepositoriesQuery.Data(createValidData()))
            .build()
            .let { Observable.just(it) }

    private fun createValidData() = RepositoriesQuery.RepositoryOwner(
        "someType",
        OWNER,
        "ownerUrl",
        createValidRepositories()
    )

    private fun createValidRepositories() =
        RepositoriesQuery.Repositories(
            "type",
            3,
            createListOfRepositories(),
            RepositoriesQuery.PageInfo("type", "endCursorValue", true)
        )

    private fun createListOfRepositories() = listOf(
        RepositoriesQuery.Node("type", "name1", "url1"),
        RepositoriesQuery.Node("type", "name2", "url2"),
        RepositoriesQuery.Node("type", "name3", "url3")
    )

    companion object {
        private const val OWNER = "owner"
        private const val PAGE_SIZE = 50

        private val validResponse = UserRepositoriesData(
            owner = RepositoryOwner(OWNER, "ownerUrl"),
            repositories = RepositoriesPage(
                repositoryItems = listOf(
                    RepositoryItem("name1", "url1", OWNER),
                    RepositoryItem("name2", "url2", OWNER),
                    RepositoryItem("name3", "url3", OWNER)
                ),
                pageInfo = NextPageInfo("endCursorValue", true)
            )
        )
    }
}
