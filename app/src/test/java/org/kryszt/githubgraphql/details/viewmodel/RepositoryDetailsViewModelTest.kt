package org.kryszt.githubgraphql.details.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.apollographql.apollo.api.Response
import com.apollographql.apollo.exception.ApolloException
import com.google.common.truth.Truth
import com.nhaarman.mockitokotlin2.given
import com.nhaarman.mockitokotlin2.mock
import io.reactivex.Observable
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.koin.core.context.startKoin
import org.koin.core.parameter.parametersOf
import org.koin.test.AutoCloseKoinTest
import org.koin.test.inject
import org.koin.test.mock.declareMock
import org.kryszt.githubgraphql.RepositoryDetailsQuery
import org.kryszt.githubgraphql.common.DataState
import org.kryszt.githubgraphql.common.di.commonModule
import org.kryszt.githubgraphql.details.di.detailsModule
import org.kryszt.githubgraphql.details.entities.DetailsDataSource
import org.kryszt.githubgraphql.details.entities.RepositoryDetails
import org.kryszt.githubgraphql.common.network.di.networkModule
import org.kryszt.githubgraphql.common.network.error.NoContentException
import org.kryszt.githubgraphql.common.network.error.ResponseError
import org.kryszt.githubgraphql.common.network.error.UnknownErrorException
import org.kryszt.githubgraphql.repositories.di.repositoriesModule
import org.kryszt.githubgraphql.rules.TrampolineSchedulerRule
import com.apollographql.apollo.api.Error as ApolloError

class RepositoryDetailsViewModelTest : AutoCloseKoinTest() {

    @get:Rule
    val rxSchedulersRule = TrampolineSchedulerRule()
    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()


    private val sut: RepositoryDetailsViewModel by inject { parametersOf(OWNER, REPOSITORY) }

    @Before
    fun before() {
        startKoin {
            modules(listOf(commonModule, networkModule, repositoriesModule, detailsModule))
        }
    }

    @Test
    fun `should set error state on apollo error`() {
        setupMock(exceptionResponse(ApolloException("Connection error")))

        assertErrorResult {
            Truth.assertThat(throwable).isInstanceOf(ApolloException::class.java)
            Truth.assertThat(throwable.message).isEqualTo("Connection error")
        }
    }

    @Test
    fun `should set error state on error in response message`() {
        setupMock(errorResponse(ApolloError("Error Message", null, null)))

        assertErrorResult {
            Truth.assertThat(throwable).isInstanceOf(ResponseError::class.java)
            Truth.assertThat(throwable.message).isEqualTo("Error Message")
        }
    }

    @Test
    fun `should set fallback error message if one wasn't available in error`() {
        setupMock(errorResponse(ApolloError(null, null, null)))

        assertErrorResult {
            Truth.assertThat(throwable).isInstanceOf(UnknownErrorException::class.java)
        }
    }

    @Test
    fun `should set error state on empty response`() {
        setupMock(emptyResponse())

        assertErrorResult {
            Truth.assertThat(throwable).isInstanceOf(NoContentException::class.java)
        }
    }

    @Test
    fun `should set error state on response without repository`() {
        setupMock(noRepository())

        assertErrorResult {
            Truth.assertThat(throwable).isInstanceOf(NoContentException::class.java)
        }
    }

    @Test
    fun `should provide data on valid response`() {
        setupMock(withRepository())

        Truth.assertThat(sut.detailsState.value).isEqualTo(DataState.Ready(validResponse))
    }

    private fun assertErrorResult(testFunction: DataState.Error<*>.() -> Unit) {
        (sut.detailsState.value as? DataState.Error)
            ?.testFunction()
            ?: run { Assert.fail("Not an error result") }
    }

    private fun setupMock(observable: Observable<Response<RepositoryDetailsQuery.Data>>) {
        declareMock<DetailsDataSource> {
            given(loadDetails(OWNER, REPOSITORY)).will { observable }
        }
    }

    private fun exceptionResponse(error: Throwable): Observable<Response<RepositoryDetailsQuery.Data>> =
        Observable.error(error)

    private fun errorResponse(error: ApolloError): Observable<Response<RepositoryDetailsQuery.Data>> =
        Response.builder<RepositoryDetailsQuery.Data>(mock())
            .errors(listOf(error))
            .build()
            .let { Observable.just(it) }

    private fun emptyResponse(): Observable<Response<RepositoryDetailsQuery.Data>> =
        Response.builder<RepositoryDetailsQuery.Data>(mock())
            .data(null)
            .build()
            .let { Observable.just(it) }

    private fun noRepository(): Observable<Response<RepositoryDetailsQuery.Data>> =
        Response.builder<RepositoryDetailsQuery.Data>(mock())
            .data(RepositoryDetailsQuery.Data(null))
            .build()
            .let { Observable.just(it) }

    private fun withRepository(): Observable<Response<RepositoryDetailsQuery.Data>> =
        Response.builder<RepositoryDetailsQuery.Data>(mock())
            .data(RepositoryDetailsQuery.Data(createRepository()))
            .build()
            .let { Observable.just(it) }

    private fun createRepository() = RepositoryDetailsQuery.Repository(
        "someType",
        "repositoryName",
        RepositoryDetailsQuery.Owner("type", "userName"),
        RepositoryDetailsQuery.OpenIssues("type", 1),
        RepositoryDetailsQuery.ClosedIssues("type", 2),
        RepositoryDetailsQuery.OpenPullRequests("type", 3),
        RepositoryDetailsQuery.ClosedPullRequests("type", 4)
    )

    companion object {
        private const val OWNER = "owner"
        private const val REPOSITORY = "repository"

        private val validResponse = RepositoryDetails(
            name = "repositoryName",
            openIssues = 1,
            closedIssues = 2,
            openPullRequests = 3,
            closedPullRequests = 4
        )
    }
}
