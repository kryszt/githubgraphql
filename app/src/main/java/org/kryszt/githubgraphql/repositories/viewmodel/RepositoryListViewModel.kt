package org.kryszt.githubgraphql.repositories.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import org.kryszt.githubgraphql.common.DataState
import org.kryszt.githubgraphql.common.DisposableViewModel
import org.kryszt.githubgraphql.common.domain.ErrorState
import org.kryszt.githubgraphql.common.domain.LoadingState
import org.kryszt.githubgraphql.common.domain.ReadyState
import org.kryszt.githubgraphql.common.extensions.addTo
import org.kryszt.githubgraphql.common.extensions.isNotNullOrEmpty
import org.kryszt.githubgraphql.repositories.entities.GithubListRepository
import org.kryszt.githubgraphql.repositories.domain.PagingViewState
import org.kryszt.githubgraphql.repositories.domain.ReviewListDomainData
import org.kryszt.githubgraphql.repositories.domain.ReviewListDomainStateConverter
import org.kryszt.githubgraphql.repositories.entities.UserRepositoriesData

//Made open to allow mocking.
open class RepositoryListViewModel(private val repository: GithubListRepository, owner: String?) : DisposableViewModel() {

    private val _repositoriesState = MutableLiveData<RepositoriesState>().apply { value = (DataState.Initial()) }

    private val stateMerger = StateMerger()
    private val domainConverter = ReviewListDomainStateConverter()

    val loadingState: LiveData<LoadingState> =
        Transformations.map(_repositoriesState, domainConverter::makeLoadingState)
    val errorState: LiveData<ErrorState> = Transformations.map(_repositoriesState, domainConverter::makeErrorState)
    val readyState: LiveData<ReadyState<ReviewListDomainData>> =
        Transformations.map(_repositoriesState, domainConverter::makeReadyState)
    val pagingState: LiveData<PagingViewState> =
        Transformations.map(_repositoriesState, domainConverter::makeNextPageState)

    open val repositoriesState: LiveData<RepositoriesState>
        get() = _repositoriesState

    init {
        //FIXME - make final when tests are updated
        loadRepositories(owner)
    }

    open fun loadRepositories(owner: String?) {
        cancelOngoing()
        if (owner == null) {
            _repositoriesState.value = DataState.Error(IllegalArgumentException("Owner is required"))
            return
        }
        startLoading(owner)
    }

    private fun startLoading(owner: String) {
        _repositoriesState.value = DataState.Loading()
        handleCall(repository.loadRepositories(owner, PAGE_SIZE))
    }

    fun loadMore() {
        _repositoriesState.value
            ?.let { it as? DataState.Ready }
            ?.let { it.data }
            ?.takeIf { it.canLoadMore() }
            ?.let { it.setLoading() }
            ?.also { _repositoriesState.value = DataState.Ready(it) }
            ?.also { startLoadingMore(it) }
    }

    private fun startLoadingMore(currentState: UserRepositoriesData) {
        repository
            .loadRepositories(currentState.owner.name, PAGE_SIZE, currentState.repositories.pageInfo.endCursor)
            .also { handleCall(it) }
    }

    private fun handleCall(call: Observable<UserRepositoriesData>) {
        call.subscribeOn(Schedulers.io())
            .map<RepositoriesState> { DataState.Ready(it) }
            .onErrorReturn { DataState.Error(it) }
            .observeOn(AndroidSchedulers.mainThread())
            .map { stateMerger.combineStates(_repositoriesState.value, it) }
            .subscribe { _repositoriesState.value = it }
            .addTo(this)
    }

    private fun UserRepositoriesData.canLoadMore() = hasNextPage() && !isLoadingMore()
    private fun UserRepositoriesData.isLoadingMore() = repositories.pageInfo.isLoading
    private fun UserRepositoriesData.hasNextPage() =
        repositories.pageInfo.hasNextPage
                && repositories.pageInfo.endCursor.isNotNullOrEmpty()

    private fun UserRepositoriesData.setLoading() =
        copy(repositories = repositories.copy(pageInfo = repositories.pageInfo.copy(isLoading = true)))


    companion object {
        private const val PAGE_SIZE = 50
    }
}
