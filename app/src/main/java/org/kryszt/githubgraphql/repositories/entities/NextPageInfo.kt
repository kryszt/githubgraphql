package org.kryszt.githubgraphql.repositories.entities

data class NextPageInfo(val endCursor: String?, val hasNextPage: Boolean, val isLoading: Boolean = false)
