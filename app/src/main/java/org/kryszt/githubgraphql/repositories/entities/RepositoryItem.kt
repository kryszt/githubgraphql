package org.kryszt.githubgraphql.repositories.entities

data class RepositoryItem(val repositoryName: String, val repositoryUrl: String, val ownerName:String)
