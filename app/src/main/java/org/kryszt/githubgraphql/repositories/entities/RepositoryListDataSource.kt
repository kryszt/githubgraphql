package org.kryszt.githubgraphql.repositories.entities

import com.apollographql.apollo.ApolloClient
import com.apollographql.apollo.api.Response
import com.apollographql.apollo.rx2.Rx2Apollo
import io.reactivex.Observable
import org.kryszt.githubgraphql.RepositoriesQuery

class RepositoryListDataSource(private val apolloClient: ApolloClient) {

    fun loadRepositories(owner: String, limit: Int, loadAfter: String?)
            : Observable<Response<RepositoriesQuery.Data>> =
        RepositoriesQuery.builder()
            .name(owner)
            .limit(limit)
            .repositoriesAfter(loadAfter)
            .build()
            .let { apolloClient.query(it) }
            .let { Rx2Apollo.from(it) }

}
