package org.kryszt.githubgraphql.repositories.view

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import org.kryszt.githubgraphql.R
import org.kryszt.githubgraphql.repositories.domain.PagingViewState
import org.kryszt.githubgraphql.repositories.domain.RepositoryViewItem

class RepositoriesAdapter(private val onItemClick: (RepositoryViewItem) -> Unit, private val onLoadMore: () -> Unit) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val pagingStatusPosition: Int
        get() = itemCount - 1

    var nextPageState: PagingViewState = PagingViewState.NotReady
        set(value) {
            field = value
            notifyItemChanged(pagingStatusPosition)
        }

    var items: List<RepositoryViewItem> = emptyList()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun getItemViewType(position: Int): Int {
        return items.getOrNull(position)?.let { TYPE_REPOSITORY } ?: TYPE_NEXT_PAGE
    }

    override fun getItemCount(): Int = items.size + 1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            TYPE_REPOSITORY -> repositoryViewHolder(parent)
            TYPE_NEXT_PAGE -> nextPageViewHolder(parent)
            else -> throw IllegalStateException(viewType.toString())
        }
    }

    private fun repositoryViewHolder(parent: ViewGroup): RepositoryItemViewHolder {
        return RepositoryItemViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.repository_list_item, parent, false),
            onItemClick
        )
    }

    private fun nextPageViewHolder(parent: ViewGroup): NextPageViewHolder {
        return NextPageViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.next_page_list_item, parent, false),
            onLoadMore
        )
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is RepositoryItemViewHolder -> holder.bindItem(items[position])
            is NextPageViewHolder -> holder.bindItem(nextPageState)
        }
    }

    companion object {
        private const val TYPE_REPOSITORY = 0
        private const val TYPE_NEXT_PAGE = 1
    }
}
