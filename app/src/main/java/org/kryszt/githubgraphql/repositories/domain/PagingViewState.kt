package org.kryszt.githubgraphql.repositories.domain

import android.view.View

sealed class PagingViewState(val loadingVisibility: Int, val hasMoreVisibility: Int) {
    object NotReady : PagingViewState(
        View.GONE,
        View.GONE
    )
    object AllLoaded : PagingViewState(
        View.GONE,
        View.GONE
    )
    object LoadingMore : PagingViewState(
        View.VISIBLE,
        View.GONE
    )
    object CanLoadMore : PagingViewState(
        View.GONE,
        View.VISIBLE
    )
}
