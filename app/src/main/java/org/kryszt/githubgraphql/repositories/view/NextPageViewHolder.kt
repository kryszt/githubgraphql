package org.kryszt.githubgraphql.repositories.view

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import org.kryszt.githubgraphql.R
import org.kryszt.githubgraphql.repositories.domain.PagingViewState

class NextPageViewHolder(view: View, onLoadMoreClicked: () -> Unit) : RecyclerView.ViewHolder(view) {
    private val progressView = view.findViewById<View>(R.id.viewLoading)
    private val loadMore = view.findViewById<View>(R.id.viewLoadMore)

    init {
        loadMore.setOnClickListener { onLoadMoreClicked() }
    }

    fun bindItem(item: PagingViewState) {
        progressView.visibility = item.loadingVisibility
        loadMore.visibility = item.hasMoreVisibility
    }
}
