package org.kryszt.githubgraphql.repositories.entities

import org.kryszt.githubgraphql.RepositoriesQuery

class RepositoriesResponseMapper {

    fun mapResponse(owner: RepositoriesQuery.RepositoryOwner): UserRepositoriesData {
        return UserRepositoriesData(
            owner = RepositoryOwner(name = owner.login(), avatarUrl = owner.avatarUrl()),
            repositories = owner.repositories().toRepositoriesPage(owner.login())
        )
    }

    private fun RepositoriesQuery.Repositories.toRepositoriesPage(ownerName: String): RepositoriesPage =
        RepositoriesPage(
            repositoryItems = nodes()?.map { it.toRepositoryItem(ownerName) }.orEmpty(),
            pageInfo = pageInfo().toNextPageInfo()
        )

    private fun RepositoriesQuery.Node.toRepositoryItem(ownerName: String): RepositoryItem =
        RepositoryItem(name(), url(), ownerName)

    private fun RepositoriesQuery.PageInfo.toNextPageInfo() = NextPageInfo(endCursor(), hasNextPage())
}

