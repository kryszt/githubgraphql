package org.kryszt.githubgraphql.repositories.view

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import org.kryszt.githubgraphql.R
import org.kryszt.githubgraphql.repositories.domain.RepositoryViewItem

class RepositoryItemViewHolder(view: View, private val onClick: (RepositoryViewItem) -> Unit) :
    RecyclerView.ViewHolder(view) {
    private val textName: TextView = view.findViewById(R.id.textRepositoryName)
    private val textUrl: TextView = view.findViewById(R.id.textRepositoryUrl)

    fun bindItem(item: RepositoryViewItem) {
        textName.text = item.repositoryName
        textUrl.text = item.url
        itemView.setOnClickListener { onClick(item) }
    }
}
