package org.kryszt.githubgraphql.repositories.viewmodel

import org.kryszt.githubgraphql.common.DataState
import org.kryszt.githubgraphql.repositories.entities.UserRepositoriesData

class StateMerger {

    fun combineStates(oldState: RepositoriesState?, incomingState: RepositoriesState): RepositoriesState {
        return when (oldState) {
            is DataState.Ready -> combineWithCurrent(oldState.data, incomingState)
            else -> incomingState
        }
    }

    private fun combineWithCurrent(data: UserRepositoriesData, incomingState: RepositoriesState): RepositoriesState {
        return when (incomingState) {
            is DataState.Ready -> combineRepositories(data, incomingState.data)
            else -> DataState.Ready(data.asNotLoading())
        }
    }

    private fun combineRepositories(current: UserRepositoriesData, update: UserRepositoriesData): RepositoriesState {
        return UserRepositoriesData(
            owner = update.owner,
            repositories = update.repositories.copy(repositoryItems = current.repositories.repositoryItems + update.repositories.repositoryItems)
        ).toResult()
    }

    private fun UserRepositoriesData.asNotLoading() =
        copy(repositories = repositories.copy(pageInfo = repositories.pageInfo.copy(isLoading = false)))

    private fun UserRepositoriesData.toResult() = DataState.Ready<UserRepositoriesData>(this)
}
