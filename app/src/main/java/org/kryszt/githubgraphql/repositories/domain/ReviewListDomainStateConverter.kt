package org.kryszt.githubgraphql.repositories.domain

import org.kryszt.githubgraphql.common.DataState
import org.kryszt.githubgraphql.common.domain.DomainStateConverter
import org.kryszt.githubgraphql.common.domain.ReadyState
import org.kryszt.githubgraphql.repositories.entities.NextPageInfo
import org.kryszt.githubgraphql.repositories.entities.RepositoryItem
import org.kryszt.githubgraphql.repositories.entities.UserRepositoriesData

class ReviewListDomainStateConverter : DomainStateConverter() {

    fun makeReadyState(state: DataState<UserRepositoriesData>): ReadyState<ReviewListDomainData> = when (state) {
        is DataState.Ready -> ReadyState.Ready(mapReadyState(state.data))
        else -> ReadyState.NotReady()
    }

    fun makeNextPageState(state: DataState<UserRepositoriesData>): PagingViewState = when (state) {
        is DataState.Ready -> state.data.repositories.pageInfo.toPageViewState()
        else -> PagingViewState.NotReady
    }

    private fun mapReadyState(data: UserRepositoriesData): ReviewListDomainData {
        return ReviewListDomainData(
            user = UserViewItem(ownerName = data.owner.name, ownerAvatar = data.owner.avatarUrl),
            items = data.repositories.repositoryItems.map { it.toViewItem() }
        )
    }

    private fun RepositoryItem.toViewItem(): RepositoryViewItem =
        RepositoryViewItem(
            repositoryName = repositoryName,
            url = repositoryUrl,
            ownerName = ownerName
        )

    private fun NextPageInfo.toPageViewState(): PagingViewState =
        when {
            isLoading -> PagingViewState.LoadingMore
            hasNextPage -> PagingViewState.CanLoadMore
            else -> PagingViewState.AllLoaded
        }
}
