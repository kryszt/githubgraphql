package org.kryszt.githubgraphql.repositories.entities

data class UserRepositoriesData(
    val owner: RepositoryOwner,
    val repositories: RepositoriesPage
)
