package org.kryszt.githubgraphql.repositories.domain

data class ReviewListDomainData(val user: UserViewItem, val items: List<RepositoryViewItem>)

data class UserViewItem(val ownerName: String, val ownerAvatar: String)

data class RepositoryViewItem(val repositoryName: String, val url: String, val ownerName: String)

