package org.kryszt.githubgraphql.repositories.view

enum class NextPageState { HAS_MORE, LOADING, NO_MORE }
