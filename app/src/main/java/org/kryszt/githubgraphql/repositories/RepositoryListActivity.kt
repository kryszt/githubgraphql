package org.kryszt.githubgraphql.repositories

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.widget.EditText
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_repository_list.*
import kotlinx.android.synthetic.main.repositories_view.*
import org.koin.android.ext.android.inject
import org.koin.android.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf
import org.kryszt.githubgraphql.R
import org.kryszt.githubgraphql.common.AppNavigator
import org.kryszt.githubgraphql.common.extensions.observeNotNull
import org.kryszt.githubgraphql.repositories.domain.RepositoryViewItem
import org.kryszt.githubgraphql.repositories.domain.ReviewListDomainData
import org.kryszt.githubgraphql.repositories.domain.UserViewItem
import org.kryszt.githubgraphql.repositories.view.RepositoriesAdapter
import org.kryszt.githubgraphql.repositories.viewmodel.RepositoryListViewModel

class RepositoryListActivity : AppCompatActivity() {

    private val ownerName: String? by lazy { intent.data?.pathSegments?.firstOrNull() }
    private val appNavigator: AppNavigator by inject()

    private val listViewModel: RepositoryListViewModel by viewModel { parametersOf(ownerName ?: "octocat") }
    private lateinit var repositoriesAdapter: RepositoriesAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_repository_list)

        initRecyclerView()
        initErrorView()

        initResultObservation()
    }

    private fun initRecyclerView() {
        repositoriesAdapter = RepositoriesAdapter(onItemClick = this::handleItemClick, onLoadMore = { loadMore() })
            .also { repositoriesRecycler.adapter = it }
    }

    private fun initErrorView() {
        errorView.setOnClickListener { requestNewUser("") }
    }

    private fun handleItemClick(repository: RepositoryViewItem) {
        startActivity(appNavigator.detailsIntent(repository.ownerName, repository.repositoryName))
    }

    private fun loadMore() {
        listViewModel.loadMore()
    }

    private fun initResultObservation() {
        listViewModel.errorState.observeNotNull(this) {
            errorView.visibility = it.visibility
            errorView.errorText = it.description
        }

        listViewModel.loadingState.observeNotNull(this) {
            loadingProgress.visibility = it.visibility
        }

        listViewModel.pagingState.observeNotNull(this) {
            repositoriesAdapter.nextPageState = it
        }

        listViewModel.readyState.observeNotNull(this) {
            contentView.visibility = it.visibility
            it.data?.let(this::displayData)
        }
    }

    private fun displayData(data: ReviewListDomainData) {
        showUser(data.user)
        showRepositories(data.items)
    }

    private fun showUser(owner: UserViewItem) {
        textOwnerName.text = owner.ownerName
        Picasso.get().load(owner.ownerAvatar).into(imageOwnerAvatar)
        imageEdit.setOnClickListener { requestNewUser(owner.ownerName) }
    }

    private fun requestNewUser(name: String) {
        val input = createNameInput().apply {
            setText(name)
            selectAll()
        }

        AlertDialog.Builder(this)
            .setTitle(getString(R.string.user_selection_dialog_title))
            .setView(input)
            .setPositiveButton(android.R.string.ok) { _, _ -> applyNewUser(input.text.toString()) }
            .setCancelable(true)
            .show()
    }

    @SuppressLint("InflateParams")
    private fun createNameInput(): EditText =
        LayoutInflater.from(this)
            .inflate(R.layout.user_input_view, null, false)
            .findViewById(R.id.nameInput)

    private fun applyNewUser(newUserName: String) {
        listViewModel.loadRepositories(newUserName)
    }

    private fun showRepositories(repositories: List<RepositoryViewItem>) {
        repositoriesAdapter.items = repositories
    }

}
