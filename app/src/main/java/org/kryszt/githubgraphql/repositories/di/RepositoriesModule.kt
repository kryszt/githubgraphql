package org.kryszt.githubgraphql.repositories.di

import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module
import org.kryszt.githubgraphql.repositories.entities.GithubListRepository
import org.kryszt.githubgraphql.repositories.entities.RepositoryListDataSource
import org.kryszt.githubgraphql.repositories.viewmodel.RepositoryListViewModel

val repositoriesModule = module {
    viewModel { (ownerName: String?) -> RepositoryListViewModel(repository = get(), owner = ownerName) }

    single {
        RepositoryListDataSource(apolloClient = get())
    }

    single {
        // current implementation doesn't need a singleton here. But it would be useful if we do some caching
        GithubListRepository(apolloDataSource = get())
    }
}

