package org.kryszt.githubgraphql.repositories.entities

import com.apollographql.apollo.api.Response
import io.reactivex.Observable
import org.kryszt.githubgraphql.RepositoriesQuery
import org.kryszt.githubgraphql.common.network.error.NoContentException
import org.kryszt.githubgraphql.common.network.error.ResponseError
import org.kryszt.githubgraphql.common.network.error.UnknownErrorException

class GithubListRepository(private val apolloDataSource: RepositoryListDataSource) {

    private val repositoriesResponseMapper by lazy { RepositoriesResponseMapper() }

    fun loadRepositories(owner: String, pageSize: Int, loadAfter: String? = null): Observable<UserRepositoriesData> =
        apolloDataSource
            .loadRepositories(owner, pageSize, loadAfter)
            .flatMap { filterErrors(it) }
            .flatMap(::mapOwner)
            .map(repositoriesResponseMapper::mapResponse)

    private fun mapOwner(data: RepositoriesQuery.Data): Observable<RepositoriesQuery.RepositoryOwner> =
        data
            .repositoryOwner()
            ?.let { Observable.just(it) }
            ?: Observable.error(NoContentException())

    private fun <T> filterErrors(response: Response<T>): Observable<T> {
        return if (response.hasErrors()) {
            response
                .errors()
                .firstOrNull()
                ?.message()
                ?.let { Observable.error<T>(ResponseError(it)) }
                ?: Observable.error(UnknownErrorException())
        } else {
            response
                .data()
                ?.let { Observable.just(it) }
                ?: Observable.error(NoContentException())
        }
    }
}
