package org.kryszt.githubgraphql.repositories.viewmodel

import org.kryszt.githubgraphql.common.DataState
import org.kryszt.githubgraphql.repositories.entities.UserRepositoriesData

typealias RepositoriesState = DataState<UserRepositoriesData>
