package org.kryszt.githubgraphql.repositories.entities

data class RepositoryOwner(val name: String, val avatarUrl: String)
