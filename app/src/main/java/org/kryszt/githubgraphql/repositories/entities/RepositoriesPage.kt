package org.kryszt.githubgraphql.repositories.entities

data class RepositoriesPage(
    val repositoryItems: List<RepositoryItem>,
    val pageInfo: NextPageInfo
)
