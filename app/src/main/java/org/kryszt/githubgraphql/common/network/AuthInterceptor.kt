package org.kryszt.githubgraphql.common.network

import okhttp3.Interceptor
import okhttp3.Response

class AuthInterceptor(private val authToken:String) : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val original = chain.request()
        return original
            .newBuilder()
            .method(original.method, original.body)
            .addHeader("Authorization", "bearer $authToken")
            .build()
            .let { chain.proceed(it) }
    }
}
