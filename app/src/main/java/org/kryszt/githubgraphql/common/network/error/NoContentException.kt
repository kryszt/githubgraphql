package org.kryszt.githubgraphql.common.network.error

import com.apollographql.apollo.exception.ApolloException

class NoContentException : ApolloException("Response had no content")
