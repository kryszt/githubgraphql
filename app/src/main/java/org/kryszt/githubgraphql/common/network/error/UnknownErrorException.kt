package org.kryszt.githubgraphql.common.network.error

import com.apollographql.apollo.exception.ApolloException

class UnknownErrorException : ApolloException("No error errorState in response")
