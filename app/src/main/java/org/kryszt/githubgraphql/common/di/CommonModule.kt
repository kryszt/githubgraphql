package org.kryszt.githubgraphql.common.di

import org.koin.dsl.module
import org.kryszt.githubgraphql.common.AppNavigator

val commonModule = module {
    single { AppNavigator() }
}
