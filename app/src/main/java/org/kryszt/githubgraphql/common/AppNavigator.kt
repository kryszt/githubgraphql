package org.kryszt.githubgraphql.common

import android.content.Intent
import android.net.Uri

class AppNavigator {

    fun detailsIntent(owner: String, repository: String): Intent =
        uriBase(HOST_DETAILS)
            .appendPath(owner)
            .appendPath(repository)
            .build()
            .let { Intent(Intent.ACTION_VIEW, it) }

    private fun uriBase(host: String): Uri.Builder =
        Uri.Builder()
            .scheme(SCHEME)
            .authority(host)

    companion object {
        private const val SCHEME = "githubgraphql"
        private const val HOST_DETAILS = "details"
    }
}
