package org.kryszt.githubgraphql.common

sealed class DataState<T> {
    data class Initial<T>(private val data: T? = null) : DataState<T>()
    data class Loading<T>(private val data: T? = null) : DataState<T>()
    data class Error<T>(val throwable: Throwable) : DataState<T>()
    data class Ready<T>(val data: T) : DataState<T>()
}
