package org.kryszt.githubgraphql.common.domain

import android.view.View

sealed class LoadingState(val visibility: Int) {
    object NotLoading : LoadingState(View.GONE)
    object Loading : LoadingState(View.VISIBLE)
}
