package org.kryszt.githubgraphql.common.extensions

fun String?.isNotNullOrEmpty(): Boolean = !this.isNullOrEmpty()
