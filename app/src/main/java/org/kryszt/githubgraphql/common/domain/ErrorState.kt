package org.kryszt.githubgraphql.common.domain

import android.view.View

sealed class ErrorState(val visibility: Int, val description: String?) {
    object NoError : ErrorState(View.GONE, null)
    class Error(description: String?) :
        ErrorState(View.VISIBLE, description)
}
