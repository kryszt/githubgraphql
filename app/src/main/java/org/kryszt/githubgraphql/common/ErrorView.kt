package org.kryszt.githubgraphql.common

import android.content.Context
import android.util.AttributeSet
import android.view.View
import androidx.appcompat.widget.LinearLayoutCompat
import kotlinx.android.synthetic.main.error_view.view.*
import org.kryszt.githubgraphql.R

class ErrorView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : LinearLayoutCompat(context, attrs, defStyleAttr) {

    init {
        View.inflate(context, R.layout.error_view, this)
        orientation = VERTICAL
    }

    var errorText: CharSequence?
        get() = textErrorDescription.text
        set(value) {
            textErrorDescription.text = value
        }
}
