package org.kryszt.githubgraphql.common.network.error

import com.apollographql.apollo.exception.ApolloException

class ResponseError(message: String) : ApolloException(message)
