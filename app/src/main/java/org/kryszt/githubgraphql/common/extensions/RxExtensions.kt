package org.kryszt.githubgraphql.common.extensions

import io.reactivex.disposables.Disposable
import io.reactivex.internal.disposables.DisposableContainer

fun Disposable.addTo(disposables: DisposableContainer): Disposable =
    also { disposables.add(it) }
