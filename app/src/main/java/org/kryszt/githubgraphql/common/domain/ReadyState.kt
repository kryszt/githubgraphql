package org.kryszt.githubgraphql.common.domain

import android.view.View

sealed class ReadyState<T>(val visibility: Int, val data: T?) {

    class NotReady<T> : ReadyState<T>(View.GONE, null)
    class Ready<T>(data: T) : ReadyState<T>(View.VISIBLE, data)
}
