package org.kryszt.githubgraphql.common.network

data class GitClientConfig(val endpoint: String, val authToken: String)
