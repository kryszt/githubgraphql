package org.kryszt.githubgraphql.common

import androidx.lifecycle.ViewModel
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.internal.disposables.DisposableContainer

abstract class DisposableViewModel(private val disposables: CompositeDisposable = CompositeDisposable()) :
    ViewModel(), DisposableContainer by disposables {

    override fun onCleared() {
        super.onCleared()
        disposables.clear()
    }

    internal fun cancelOngoing() {
        disposables.clear()
    }
}
