package org.kryszt.githubgraphql.common.extensions

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer

fun <T> LiveData<T>.observeNotNull(lifecycleOwner: LifecycleOwner, onChange: (T) -> Unit) {
    observe(lifecycleOwner, Observer { it?.let(onChange) })
}
