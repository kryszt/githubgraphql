package org.kryszt.githubgraphql.common.network.di

import com.apollographql.apollo.ApolloClient
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.dsl.module
import org.kryszt.githubgraphql.BuildConfig
import org.kryszt.githubgraphql.common.network.AuthInterceptor
import org.kryszt.githubgraphql.common.network.GitClientConfig

val networkModule = module {

    single {
        GitClientConfig(
            "https://api.github.com/graphql",
            BuildConfig.GIT_GRAPHQL_AUTH0_TOKEN
        )
    }

    single {
        val clientConfig = get<GitClientConfig>()
        OkHttpClient.Builder()
            .addInterceptor(HttpLoggingInterceptor().apply { level = HttpLoggingInterceptor.Level.BODY })
            .addInterceptor(AuthInterceptor(clientConfig.authToken))
            .build()
    }

    single {
        val clientConfig = get<GitClientConfig>()
        ApolloClient
            .builder()
            .okHttpClient(get())
            .serverUrl(clientConfig.endpoint)
            .build()
    }
}
