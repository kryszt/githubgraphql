package org.kryszt.githubgraphql.common.domain

import org.kryszt.githubgraphql.common.DataState

open class DomainStateConverter {
    fun <T> makeLoadingState(state: DataState<T>): LoadingState = when (state) {
        is DataState.Initial<*>, is DataState.Loading<*> -> LoadingState.Loading
        else -> LoadingState.NotLoading
    }

    fun <T> makeErrorState(state: DataState<T>): ErrorState = when (state) {
        is DataState.Error<*> -> ErrorState.Error(state.throwable.message)
        else -> ErrorState.NoError
    }
}
