package org.kryszt.githubgraphql

import android.app.Application
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import org.kryszt.githubgraphql.common.di.commonModule
import org.kryszt.githubgraphql.details.di.detailsModule
import org.kryszt.githubgraphql.common.network.di.networkModule
import org.kryszt.githubgraphql.repositories.di.repositoriesModule

class GitClientApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@GitClientApplication)
            modules(listOf(commonModule, networkModule, repositoriesModule, detailsModule))
        }
    }
}
