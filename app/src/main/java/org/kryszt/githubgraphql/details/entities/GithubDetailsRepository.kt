package org.kryszt.githubgraphql.details.entities

import com.apollographql.apollo.api.Response
import io.reactivex.Observable
import org.kryszt.githubgraphql.RepositoryDetailsQuery
import org.kryszt.githubgraphql.common.network.error.NoContentException
import org.kryszt.githubgraphql.common.network.error.ResponseError
import org.kryszt.githubgraphql.common.network.error.UnknownErrorException

class GithubDetailsRepository(private val apolloDataSource: DetailsDataSource) {

    private val detailsResponseMapper by lazy { DetailsResponseMapper() }

    fun loadDetails(owner: String, repositoryName: String): Observable<RepositoryDetails> =
        apolloDataSource
            .loadDetails(owner, repositoryName)
            .flatMap { filterErrors(it) }
            .flatMap(::mapRepository)
            .map(detailsResponseMapper::mapResponse)

    private fun mapRepository(data: RepositoryDetailsQuery.Data): Observable<RepositoryDetailsQuery.Repository> =
        data
            .repository()
            ?.let { Observable.just(it) }
            ?: Observable.error(NoContentException())

    private fun <T> filterErrors(response: Response<T>): Observable<T> {
        return if (response.hasErrors()) {
            response
                .errors()
                .firstOrNull()
                ?.message()
                ?.let { Observable.error<T>(ResponseError(it)) }
                ?: Observable.error(UnknownErrorException())
        } else {
            response
                .data()
                ?.let { Observable.just(it) }
                ?: Observable.error(NoContentException())
        }
    }

}
