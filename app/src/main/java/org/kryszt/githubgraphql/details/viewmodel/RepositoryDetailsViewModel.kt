package org.kryszt.githubgraphql.details.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import org.kryszt.githubgraphql.common.DataState
import org.kryszt.githubgraphql.common.DisposableViewModel
import org.kryszt.githubgraphql.common.domain.ErrorState
import org.kryszt.githubgraphql.common.domain.LoadingState
import org.kryszt.githubgraphql.common.domain.ReadyState
import org.kryszt.githubgraphql.common.extensions.addTo
import org.kryszt.githubgraphql.details.domain.RepositoryDetailsDomain
import org.kryszt.githubgraphql.details.domain.RepositoryDetailsDomainStateConverter
import org.kryszt.githubgraphql.details.entities.GithubDetailsRepository

open class RepositoryDetailsViewModel(
    private val repository: GithubDetailsRepository,
    owner: String?,
    repositoryName: String?
) : DisposableViewModel() {

    private val _detailsState = MutableLiveData<RepositoryDetailsState>()
    private val domainConverter = RepositoryDetailsDomainStateConverter()

    val loadingState: LiveData<LoadingState> =
        Transformations.map(_detailsState, domainConverter::makeLoadingState)
    val errorState: LiveData<ErrorState> = Transformations.map(_detailsState, domainConverter::makeErrorState)

    val details: LiveData<ReadyState<RepositoryDetailsDomain>> =
        Transformations.map(_detailsState, domainConverter::makeReadyState)

    open val detailsState: LiveData<RepositoryDetailsState>
        get() = _detailsState

    init {
        //FIXME - make the class final again
        loadDetails(owner, repositoryName)
    }

    open fun loadDetails(owner: String?, repositoryName: String?) {
        cancelOngoing()
        if (owner == null) {
            _detailsState.value = DataState.Error(IllegalArgumentException("Owner is required"))
            return
        }
        if (repositoryName == null) {
            _detailsState.value = DataState.Error(IllegalArgumentException("Repository name is required"))
            return
        }
        startLoading(owner, repositoryName)
    }

    private fun startLoading(owner: String, repositoryName: String) {
        _detailsState.value = DataState.Loading()
        repository
            .loadDetails(owner, repositoryName)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .map<RepositoryDetailsState> { DataState.Ready(it) }
            .onErrorReturn { DataState.Error(it) }
            .subscribe { _detailsState.value = it }
            .addTo(this)
    }
}
