package org.kryszt.githubgraphql.details.domain

data class RepositoryDetailsDomain(
    val name: String,
    val openIssues: String,
    val closedIssues: String,
    val openPullRequests: String,
    val closedPullRequests: String
)
