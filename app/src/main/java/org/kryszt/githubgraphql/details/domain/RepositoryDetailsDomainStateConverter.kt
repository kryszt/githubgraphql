package org.kryszt.githubgraphql.details.domain

import org.kryszt.githubgraphql.common.DataState
import org.kryszt.githubgraphql.common.domain.DomainStateConverter
import org.kryszt.githubgraphql.common.domain.ReadyState
import org.kryszt.githubgraphql.details.entities.RepositoryDetails

class RepositoryDetailsDomainStateConverter : DomainStateConverter() {

    fun makeReadyState(state: DataState<RepositoryDetails>): ReadyState<RepositoryDetailsDomain> = when (state) {
        is DataState.Ready -> ReadyState.Ready(mapReadyState(state.data))
        else -> ReadyState.NotReady()
    }

    private fun mapReadyState(data: RepositoryDetails): RepositoryDetailsDomain {
        return RepositoryDetailsDomain(
            name = data.name,
            openIssues = data.openIssues.toString(),
            closedIssues = data.closedIssues.toString(),
            openPullRequests = data.openPullRequests.toString(),
            closedPullRequests = data.closedPullRequests.toString()
        )
    }
}
