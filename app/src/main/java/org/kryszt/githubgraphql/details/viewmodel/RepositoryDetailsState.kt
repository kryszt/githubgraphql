package org.kryszt.githubgraphql.details.viewmodel

import org.kryszt.githubgraphql.common.DataState
import org.kryszt.githubgraphql.details.entities.RepositoryDetails

typealias RepositoryDetailsState = DataState<RepositoryDetails>
