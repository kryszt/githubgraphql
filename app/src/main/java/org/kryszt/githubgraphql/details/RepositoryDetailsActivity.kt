package org.kryszt.githubgraphql.details

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_details.*
import org.koin.android.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf
import org.kryszt.githubgraphql.R
import org.kryszt.githubgraphql.common.extensions.observeNotNull
import org.kryszt.githubgraphql.details.domain.RepositoryDetailsDomain
import org.kryszt.githubgraphql.details.viewmodel.RepositoryDetailsViewModel

class RepositoryDetailsActivity : AppCompatActivity() {
    private val userName: String? by lazy { intent.data?.pathSegments?.firstOrNull() }
    private val repositoryName: String? by lazy { intent.data?.pathSegments?.getOrNull(1) }

    private val detailsViewModel: RepositoryDetailsViewModel by viewModel { parametersOf(userName, repositoryName) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details)

        observeViewModel()
    }

    private fun observeViewModel() {
        detailsViewModel.errorState.observeNotNull(this) {
            errorView.errorText = it.description
            errorView.visibility = it.visibility
        }

        detailsViewModel.loadingState.observeNotNull(this) {
            loadingProgress.visibility = it.visibility
        }

        detailsViewModel.details.observeNotNull(this) {
            repositoryDetailsView.visibility = it.visibility
            it.data?.let(this::displayData)
        }
    }

    private fun displayData(data: RepositoryDetailsDomain) {
        with(repositoryDetailsView) {
            setRepositoryName(data.name)
            setOpenIssuesCount(data.openIssues)
            setClosedIssuesCount(data.closedIssues)
            setOpenPullRequestsCount(data.openPullRequests)
            setClosedPullRequestsCount(data.closedPullRequests)
        }
    }
}
