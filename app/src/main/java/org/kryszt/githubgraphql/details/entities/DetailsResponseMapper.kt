package org.kryszt.githubgraphql.details.entities

import org.kryszt.githubgraphql.RepositoryDetailsQuery
import org.kryszt.githubgraphql.details.entities.RepositoryDetails

class DetailsResponseMapper {

    fun mapResponse(repository: RepositoryDetailsQuery.Repository): RepositoryDetails {
        return RepositoryDetails(
            name = repository.name(),
            openIssues = repository.openIssues().totalCount(),
            closedIssues = repository.closedIssues().totalCount(),
            openPullRequests = repository.openPullRequests().totalCount(),
            closedPullRequests = repository.closedPullRequests().totalCount()
        )
    }
}
