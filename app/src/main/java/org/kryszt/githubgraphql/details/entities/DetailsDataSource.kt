package org.kryszt.githubgraphql.details.entities

import com.apollographql.apollo.ApolloClient
import com.apollographql.apollo.api.Response
import com.apollographql.apollo.rx2.Rx2Apollo
import io.reactivex.Observable
import org.kryszt.githubgraphql.RepositoryDetailsQuery

class DetailsDataSource(private val apolloClient: ApolloClient) {

    fun loadDetails(owner: String, repositoryName: String): Observable<Response<RepositoryDetailsQuery.Data>> =
        RepositoryDetailsQuery.builder()
            .owner(owner)
            .repositoryName(repositoryName)
            .build()
            .let { apolloClient.query(it) }
            .let { Rx2Apollo.from(it) }
}
