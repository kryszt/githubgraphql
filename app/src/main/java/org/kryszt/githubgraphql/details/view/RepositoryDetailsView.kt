package org.kryszt.githubgraphql.details.view

import android.content.Context
import android.util.AttributeSet
import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout
import kotlinx.android.synthetic.main.repository_details_view.view.*
import org.kryszt.githubgraphql.R

class RepositoryDetailsView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr) {

    init {
        View.inflate(context, R.layout.repository_details_view, this)
    }

    fun setRepositoryName(name: String) {
        textRepositoryName.text = name
    }

    fun setOpenIssuesCount(count: String) {
        textOpenIssues.text = count
    }

    fun setClosedIssuesCount(count: String) {
        textClosedIssues.text = count
    }

    fun setOpenPullRequestsCount(count: String) {
        textOpenPullRequests.text = count
    }

    fun setClosedPullRequestsCount(count: String) {
        textClosedPullRequests.text = count
    }
}
