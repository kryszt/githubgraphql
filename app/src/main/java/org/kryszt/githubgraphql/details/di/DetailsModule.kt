package org.kryszt.githubgraphql.details.di

import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module
import org.kryszt.githubgraphql.details.entities.DetailsDataSource
import org.kryszt.githubgraphql.details.entities.GithubDetailsRepository
import org.kryszt.githubgraphql.details.viewmodel.RepositoryDetailsViewModel

val detailsModule = module {
    viewModel { (owner: String?, repositoryName: String?) ->
        RepositoryDetailsViewModel(
            repository = get(),
            owner = owner,
            repositoryName = repositoryName
        )
    }

    single {
        DetailsDataSource(apolloClient = get())
    }

    single {
        // current implementation doesn't need a singleton here. But it would be useful if we do some caching
        GithubDetailsRepository(apolloDataSource = get())
    }
}
