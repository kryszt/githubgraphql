package org.kryszt.githubgraphql.details.entities

data class RepositoryDetails(
    val name: String,
    val openIssues: Int = 0,
    val closedIssues: Int = 0,
    val openPullRequests: Int = 0,
    val closedPullRequests: Int = 0
)
