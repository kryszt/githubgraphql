package org.kryszt.githubgraphql

import android.app.Application
import androidx.lifecycle.MutableLiveData
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.core.context.startKoin
import org.koin.core.qualifier.named
import org.koin.dsl.module
import org.kryszt.githubgraphql.common.di.commonModule
import org.kryszt.githubgraphql.details.viewmodel.RepositoryDetailsState
import org.kryszt.githubgraphql.details.viewmodel.RepositoryDetailsViewModel
import org.kryszt.githubgraphql.repositories.viewmodel.RepositoriesState
import org.kryszt.githubgraphql.repositories.viewmodel.RepositoryListViewModel
import org.mockito.Mockito

class TestApplication : Application() {
    override fun onCreate() {

        val mockedListViewModel = Mockito.mock(RepositoryListViewModel::class.java)
        val mockedDetailsModel = Mockito.mock(RepositoryDetailsViewModel::class.java)

        val repositoryListLiveData = MutableLiveData<RepositoriesState>()
        val repositoryDetailsLiveData = MutableLiveData<RepositoryDetailsState>()

        Mockito.`when`(mockedListViewModel.repositoriesState).thenReturn(repositoryListLiveData)
        Mockito.`when`(mockedDetailsModel.detailsState).thenReturn(repositoryDetailsLiveData)

        val moduleRepositoryList = module {
            viewModel<RepositoryListViewModel> { mockedListViewModel }
            single(named("list")) { repositoryListLiveData }
        }

        val moduleRepositoryDetails = module {
            viewModel<RepositoryDetailsViewModel> { mockedDetailsModel }
            single(named("details")) { repositoryDetailsLiveData }
        }

        startKoin {
            modules(listOf(commonModule, moduleRepositoryDetails, moduleRepositoryList))
        }
    }
}
