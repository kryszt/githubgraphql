package org.kryszt.githubgraphql

import androidx.test.espresso.ViewInteraction
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.matcher.ViewMatchers

fun ViewInteraction.isGone(): ViewInteraction =
    check(
        ViewAssertions.matches(ViewMatchers.withEffectiveVisibility(ViewMatchers.Visibility.GONE))
    )

fun ViewInteraction.isVisible(): ViewInteraction = check(ViewAssertions.matches(ViewMatchers.isDisplayed()))

fun ViewInteraction.hasText(text: String): ViewInteraction =
    check(ViewAssertions.matches(ViewMatchers.withText(text)))
