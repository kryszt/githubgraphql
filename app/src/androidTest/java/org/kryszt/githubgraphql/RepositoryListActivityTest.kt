package org.kryszt.githubgraphql

import androidx.lifecycle.MutableLiveData
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.koin.core.qualifier.named
import org.koin.test.KoinTest
import org.koin.test.inject
import org.kryszt.githubgraphql.common.DataState
import org.kryszt.githubgraphql.repositories.RepositoryListActivity
import org.kryszt.githubgraphql.repositories.entities.*
import org.kryszt.githubgraphql.repositories.viewmodel.RepositoriesState

/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4::class)
class RepositoryListActivityTest : KoinTest {

    @get:Rule
    var activityScenarioRule = ActivityScenarioRule(RepositoryListActivity::class.java)

    private val liveData: MutableLiveData<RepositoriesState> by inject(named("list"))

    @Test
    fun showErrorState() {
        liveData.postValue(DataState.Error(Exception("Some Error Message")))
        onView(withId(R.id.textErrorDescription)).check(matches(withText("Some Error Message")))
    }

    @Test
    fun showLoadingState() {
        liveData.postValue(DataState.Loading())
        onView(withId(R.id.loadingProgress)).isVisible()
        onView(withId(R.id.errorView)).isGone()
        onView(withId(R.id.contentView)).isGone()
    }

    @Test
    fun showContent() {
        val data = UserRepositoriesData(
            owner = RepositoryOwner("I am owner", "avatar"),
            repositories = RepositoriesPage(
                repositoryItems = listOf(
                    RepositoryItem("repository1", "url1", "owner"),
                    RepositoryItem("repository2", "url2", "owner")
                ),
                pageInfo = NextPageInfo("", false)
            )
        )

        liveData.postValue(DataState.Ready(data))

        onView(withId(R.id.loadingProgress)).isGone()
        onView(withId(R.id.errorView)).isGone()

        onView(withId(R.id.contentView)).isVisible()
        onView(withText("I am owner")).isVisible()

        //list items
        onView(withText("repository1")).isVisible()
        onView(withText("repository2")).isVisible()
        onView(withText("url1")).isVisible()
        onView(withText("url2")).isVisible()
    }
}
