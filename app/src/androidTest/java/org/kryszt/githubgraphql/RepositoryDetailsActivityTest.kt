package org.kryszt.githubgraphql

import androidx.lifecycle.MutableLiveData
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.koin.core.qualifier.named
import org.koin.test.KoinTest
import org.koin.test.inject
import org.kryszt.githubgraphql.common.DataState
import org.kryszt.githubgraphql.details.RepositoryDetailsActivity
import org.kryszt.githubgraphql.details.entities.RepositoryDetails
import org.kryszt.githubgraphql.details.viewmodel.RepositoryDetailsState

/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4::class)
class RepositoryDetailsActivityTest : KoinTest {

    @get:Rule
    var activityScenarioRule = ActivityScenarioRule(RepositoryDetailsActivity::class.java)

    private val liveData: MutableLiveData<RepositoryDetailsState> by inject(named("details"))

    @Test
    fun showErrorState() {
        liveData.postValue(DataState.Error(Exception("Some Error Message")))
        onView(withId(R.id.textErrorDescription)).check(matches(withText("Some Error Message")))
    }

    @Test
    fun showLoadingState() {
        liveData.postValue(DataState.Loading())
        onView(withId(R.id.loadingProgress)).isVisible()
        onView(withId(R.id.errorView)).isGone()
        onView(withId(R.id.repositoryDetailsView)).isGone()
    }

    @Test
    fun showContent() {
        val data = RepositoryDetails(
            name = "repositoryName",
            openIssues = 111,
            closedIssues = 222,
            openPullRequests = 333,
            closedPullRequests = 444
        )

        liveData.postValue(DataState.Ready(data))

        onView(withId(R.id.loadingProgress)).isGone()
        onView(withId(R.id.errorView)).isGone()

        onView(withId(R.id.repositoryDetailsView)).isVisible()
        onView(withText("repositoryName")).isVisible()

        //list items
        onView(withId(R.id.textOpenIssues)).isVisible().hasText("111")
        onView(withId(R.id.textClosedIssues)).isVisible().hasText("222")
        onView(withId(R.id.textOpenPullRequests)).isVisible().hasText("333")
        onView(withId(R.id.textClosedPullRequests)).isVisible().hasText("444")
    }

}
