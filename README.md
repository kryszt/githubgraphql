
# Build

Project requires github API key to work. 
Token is taken from property `GIT_GRAPHQL_AUTH0_TOKEN`. It can be set either in `gradle.properties` file, or passed as a build parameter:
`./gradlew assembleDebug -PGIT_GRAPHQL_AUTH0_TOKEN="\"<api_token>"\"`

# Run 

### Default
By default app opens list activity and loads repositories of `octocat` user.


### with deeplinks

Project defines two deep links:
`githubgraphql://repositories/<owner>` for activity with a list of repositories
and
`githubgraphql://details/<owner>/<repository>` for repository details

ADB can be used to start appropriate activity, i.e:

`adb shell am start -a "android.intent.action.VIEW" -d "githubgraphql://repositories/octocat"`

`adb shell am start -a "android.intent.action.VIEW" -d "githubgraphql://details/octocat/octocat.github.io"`

# Missing features
At leans the most important ones

### Proper configuration of mocks in UI tests
In order to mock ViewModels in UI tests, I decided to make the classes and mocked methods `open`.
Configuration that could allow mocking of final classes was causing conflicts among test libraries.

 
